---
title: Accueil
---

# forge.apps.education.fr

Ce site est le document d'accompagnement de la formation "Forge des Communs Numériques" proposée par le PCN NSI Normandie (juillet 2024).

Il comporte 5 parties :

- [Présentation de la forge](./01_presentation_forge/index.md)
- [Utilisation basique](./02_usage_forge_basique/index.md)
- [Créer un site en utilisant un modèle](./03_creation_site_mkdocs/index.md)
- [Point culture : DevOps, 12 factor app](./04_devops_12factor/index.md)
- [Git démystifié](./05_git_pratique/index.md)
