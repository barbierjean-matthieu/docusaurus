---
title: Utilisation
---

# TP gitlab (minus ssh???)

## Contenus

### Débuts

- créer un compte sur gitlab, créer un projet, récupérer le code, le modifier, le publier
  - clef ssh ??

### CI/CD

- faire un pipeline CI simple (qqs tests)
- déployer l'application sur un serveur (gitlab pages, ou bien mini container docker)

### Collaboration

- créer un ticket, le fermer, etc..
- lier un ticket à un commit, etc..
- merge request, cloner un projet, etc..
- gestion des conflits
