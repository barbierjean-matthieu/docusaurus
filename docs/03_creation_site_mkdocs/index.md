---
title: Créer un site
---

# Introduction

## Outils pour créer un site web statique

Plusieurs outils permettent de créer un site web statique. Parmi eux, [MkDocs](https://www.mkdocs.org/) est un outil simple et efficace pour créer un site web à partir de fichiers Markdown. Il peut être personnalisé avec des thèmes et des plugins, et est facile à déployer. [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/) est un thème très complet pour MkDocs, qui offre une navigation agréable et des fonctionnalités avancées.

## Travail simplifié

Des collègues nous ont facilité le travail en créant des sites "modèles", avec beaucoup de fonctionnalités utiles, des exemples et une documentation complète. Nous allons utiliser un de ces sites modèles pour créer notre propre site.

# Les étapes

## Cloner le site modèle

### Screens et étapes de clonage

## Explorer le site modèle et comprendre sa structure

### Dans site cloné, exploration des fichiers et dossiers

## Déployer le site

### Déclenchement du CI

### Personnalisation de l'URL

## Personnaliser le site et créer notre propre contenu

### Modification des fichiers Markdown

### Ajout de pages

### Personnalisation du thème
