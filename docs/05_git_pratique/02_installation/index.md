# Installation de git

## Sous linux

Git a de fortes chances d'être déjà installé, mais sinon (pour debian/ubuntu)

```bash
sudo apt-get install git
# Optionnallement, pour une interface graphique
sudo apt-get install git-gui
```

Pour vérifier si il est installé et si il fonctionne, ouvrir un terminal et taper :

```bash
git --version
```

## Sous MacOS

Git est distribué avec Xcode, mais vous pouvez aussi l'installer via [homebrew](https://brew.sh/)

```bash
brew install git
# Optionnallement, pour une interface graphique
brew install git-gui
```

Pour vérifier si il est installé et si il fonctionne, ouvrir un terminal et taper :

```bash
git --version
```

## Sous Windows

Git **n'est pas installé** par défaut sur Windows. Vous pouvez le télécharger et l'installer depuis le site officiel : [https://git-scm.com/](https://www.git-scm.com/download/win) (existe en version installable ou en version portable).

Installe :

- Git Bash : un terminal qui permet d'utiliser les commandes git
- Git GUI : une interface graphique pour git
- Git credential manager : pour gérer les identifiants
- Intégration avec l'explorateur de fichiers : clic droit sur un dossier pour ouvrir un terminal git

Pour vérifier si il est installé et si il fonctionne, lancez `Git Bash`, puis tapez :

```bash
git --version
```
