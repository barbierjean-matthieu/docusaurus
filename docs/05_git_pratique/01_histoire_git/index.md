# Historique

## Problème : travailler à plusieurs

- versionner son code : qui a fait quoi, quand, pourquoi, comment
- résoudre des conflits : travailler à plusieurs sur le même code, en même temps

## Solutions

### Au commencement

- moyens "primitifs" : ftp / email / disquette / clef usb
- outils de gestion de version
  - mode de stockage / accès : centralisé / décentralisé
  - mode de gestion des potentiels conflits : fusion / verrouillage

### Premiers outils

- les ancêtres - centralisés
  - cvs : centralisé / fusion
  - svn : centralisé / fusion et/ou verrouillage

### Les modernes

- historique
  - mercurial : décentralisé / fusion - création en 2005,
  - git : décentralisé / fusion - création en 2005

# Git

## Histoire de git

- 2005 : création par Linus Torvalds
  - de 1991 à 2002 : gestion du noyau Linux par email, patchs et archives
  - 2002 : BitKeeper, logiciel propriétaire, gratuit pour le noyau Linux
  - 2005 : conflit avec BitKeeper ($$$), création de git par Linus Torvalds (en 2 mois)
- 2008 : github
- 2018 : rachat par Microsoft

## État actuel

- git est aujourd'hui de-facto standard
  - github, gitlab, bitbucket, etc..
  - intégration continue, déploiement continu, etc..
  - gestion de tickets, wiki, etc..
