# Principes de git

## Caractéristiques

### Décentralisé

- chaque développeur a une copie complète de l'historique
- chaque développeur peut travailler en local
- chaque développeur peut publier ses modifications

### Fusion

- chaque développeur peut travailler en parallèle
- git gère les conflits lors de la fusion du code de plusieurs développeurs

## Fonctionnement

### Stockage des données

Stockage des données = objets :

- blobs (fichiers, nommés par le sha de leur contenu, ou bien empaquetés dans des "packs")
- trees (arborescence)
- commits (instantanés = references vers des trees)
- tags (étiquettes)

### Organisation des données

Stocke les données sous forme de "snapshots" (instantanés) de l'ensemble du code via des références / liens entre les objets.

TODO: schema

###

3 états :

- working directory : le code que vous modifiez
- staging area : les modifications que vous avez préparées pour le prochain commit
- repository : l'historique de tous les commits

TODO: diagrammes

# En pratique

## Installer git

### Installation

- linux : `sudo apt install git` (mais souvent déjà installé)
- windows : https://git-scm.com/download/win -> vient aussi avec git bash pour travailler proprement en ligne de commande
- mac : https://git-scm.com/download/mac ou `brew install git`

### Configuration

- `git config --global user.name "John Doe"`
- `git config --global user.email "john@example.com"

## Rappels SSH

### Connexion SSH

- login/mdp
- clef ssh
  - sous linux : `ssh-keygen` (RSA ou ED25519...)
  - sous windows : `ssh-keygen` dans git bash / avec putty : https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html : `puttygen`
  - sous mac : `ssh-keygen` dans terminal
- fichier `.ssh/authorized_keys` sur le serveur

## TP git classique

- voir tp ISN
- terminal only, fournir ssh pour éviter les emmerdes avec les installations
- expliquer les états (working directory, staging area, repository) + les commandes pour passer de l'un à l'autre (add, commit, etc..)
- expliquer les commandes de base (status, log, diff, etc..)
- explorer les objets sur dépôt super simple fourni (2 fichiers, qqs commits, 1 tag, 2 branches) - `git cat-file -p<hash>` / `git ls-tree <hash>`
- explorer les branches (checkout, merge, rebase, etc..)
- push / pull : clef ssh ou login/mdp
